﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.Models.User
{
    public class FriendArxiv
    {
        public int Id { get; set; }

        public string  FriendId { get; set; }
        [ForeignKey("FriendId")]
        public ApplicationUser Friend {get; set;}

        public DateTime Date { get; set; }
        public short Action { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }

    public enum FriendAction
    { Added = 1, Deleted = 2}
}
﻿<img src="http://farm5.static.flickr.com/4124/4952852845_7bdcd3e89c.jpg" width="450" height="409" alt="title14" />
<br/><br/>

<h3>Геометрия Манхэттена</h3>


На большей части Манхэттена невозможно заблудиться, умея (с)читать по-английски до двенадцати и зная арабские цифры. Если вы высадитесь севернее 14-ой улицы, то сумеете за десять секунд определить свое местонахождение, даже если у вас нет навигационного устройства, a Полярная звезда заслонена небоскребом. Для этого достаточно подойти к ближайшему перекрестку и прочитать на указателях названия пересекающихся улиц. 
<br/>

<br/>
Чаще всего это будут два числа, одно в виде порядкового числительного (Fifth Avenue - <b>Пятая</b> авеню), другое - цифрами (West 37th Street - Западная <b>37</b>-ая улица):
<br/><br/>

<a href="http://www.flickr.com/photos/ivladimir/4935897307/" title="cross by ivladimir, on Flickr"><img src="http://farm5.static.flickr.com/4119/4935897307_3cce90bb17_m.jpg" width="240" height="180" alt="cross" /></a>
<br/><br/>

И эти числа (в данном случае 5 и 37) - просто ваши координаты в идеально прямоугольной сетке авеню и улиц Манхэттена.
<br/><br/>

<b>Параллели и меридианы</b>
<br/><br/>

Авеню(Avenue) идут с юга на север, улицы (Street) - с запада на восток.
<br/><br/>

<a href="http://www.flickr.com/photos/ivladimir/4943820359/" title="ManhattanGrid_FifthAve by ivladimir, on Flickr"><img src="http://farm5.static.flickr.com/4080/4943820359_602a81897d_z.jpg" width="209" height="640" alt="ManhattanGrid_FifthAve" /></a>
<br/><br/>


<b>Авеню</b>
<br/>
Название большинства авеню - просто порядковый номер: Первая, Вторая, Третья (до Двенадцатой).
<br/>
У некоторых из них отдельные участки называются по другому (например, верхняя часть Десятой авеню - это Амстердам Авеню). Есть авеню с двойным названием (Шестая авеню, она же Авеню Америк), а также ненумерованые, с обычными названиями в честь кого-то/чего-то (Медисон Авеню).



<br/><br/>
<b>Улицы</b>. 
<br/>
В судьбе их названий определяющую роль играют  14-ая улица и Пятая авеню:
<br/>
Практически все улицы, находящиеся выше 14-ой улицы   - нумерованные, нумерация додходит до 220 и даже продолжается в Бронксе.
Находящиеся же ниже (т.е. в Downtown) существуют еще со времен голландского Нового Амстердама, идут в любом направлении и носят разнообразные названия.
<br/>
А в чем выражается влияние Пятой авеню на название улиц?
Эта срединная авеню Манхэттена делит улицы пополам.
Часть улицы к западу от Пятой авеню называется  Западная улица (West Street), а к востоку ... Восточная (East Street). 
<br/><br/>

<b>Адреса</b>
<br/>

Регулярность сетки улиц и особенности нумерции домов позволяли в доинтернетовские времена быстро решать задачу - по адресу определить место. Для этого использовалась формула, но сейчас метод устарел - есть Google. И вообще вежливость обязывает при указании формального адреса добавить информацию о квартале, где находится дом:

<br/><br/>
<a href="http://maps.google.com/maps?q=120+west+31+str.&amp;oe=utf-8&amp;rls=org.mozilla:en-US:official&amp;client=firefox-a&amp;um=1&amp;ie=UTF-8&amp;hq=&amp;hnear=120+W+31st+St,+New+York,+NY+10001&amp;gl=us&amp;ei=bax6TKy6A4P-8AbR8ryEBw&amp;sa=X&amp;oi=geocode_result&amp;ct=image&amp;resnum=1&amp;ved=0CBQQ8gEwAA" target="_blank">120 W 31 Str. (between 6th and 7th Avenue)</a>
<br/>
<i>120</i> - номер дома
<br/>
<i>W 31 Str.</i> - это West 31 Street (по причине любви англоязычников к аббревиатурам West/East сокрашается до букв W/E)
<br/>
<i>between 6th and 7th Ave.</i> - между 6-ой и 7-ой авеню
<br/><br/>
А это - адрес Эмпайр Стейт Билдинга, расположенного на Пятой авеню:
<br/>

<a href="http://maps.google.com/maps?q=350+Fifth+Avenue&amp;oe=utf-8&amp;rls=org.mozilla:en-US:official&amp;client=firefox-a&amp;um=1&amp;ie=UTF-8&amp;hq=&amp;hnear=350+5th+Ave,+New+York,+NY+10001&amp;gl=us&amp;ei=YA6BTNeSKcL98AaUz4mIAw&amp;sa=X&amp;oi=geocode_result&amp;ct=image&amp;resnum=1&amp;ved=0CBcQ8gEwAA" target="_blank">
350 Fifth Ave. (between W 33 and W 34 Str.)</a>



<br/><br/>
<b>Бродвей</b>
<br/><br/>
<i>
И страшным, страшным креном ...
<br/>
(Б.Пастернак)
</i>
<br/><br/>

Конечно же, как не упомянуть Бродвей - Великую диагональ Манхэттена.
Только посмотрите, как он себя ведет!
<br/><br/>

<a href="http://www.flickr.com/photos/ivladimir/4938167264/" title="ManhattanGrid_Broadway by ivladimir, on Flickr"><img src="http://farm5.static.flickr.com/4081/4938167264_f822222745_z.jpg" width="209" height="640" alt="ManhattanGrid_Broadway" /></a>
<br/><br/>

В броуновском движении улиц Downtown'а лишь Бродвей твердо держит путь на север, заставляя остальных равняться по нему. Когда же все авеню выстраиваются в ряд и начинают печатать шаг, Бродвей уходит влево, пока не достигнет берега острова. 
<br/>

Есть еще одна интересная особенность, при знакомстве с которой невольно вспоминаются слова Брижит Бардо, сказанные ею о своих мужьях и любовниках: "Свой час славы познал каждый мужчина, что-то значивший в моей жизни, будь он певцом, актером, плейбоем, художником или скульптором ... "
<br/>
Точно также и при встречах авеню с Бродвеем образовались всемирно известные достопримечательности. Действительно, пересечение 
<br/><br/>



<table width="567">

<tr><td>
Бродвея и Парк Авеню - Юнион Сквер
</td><td>
<img src="http://farm5.static.flickr.com/4143/4952637648_14627ed5c2_t.jpg" width="100" height="67" alt="b3jpg" />
</td></tr>


<tr><td>
Бродвея и Пятой авеню - <a href="http://igornasa.livejournal.com/627.html" target="blank">Hебоскреб Утюг</a>
</td><td>
<a href="http://igornasa.livejournal.com/627.html" target="blank">
<img src="http://farm5.static.flickr.com/4111/4951994359_ec4f6d55b0_t.jpg" width="100" height="69" alt="iron" /></a>
</td></tr>

<tr><td>
Бродвея и Шестой авеню - Геральд Сквер (Macy's)
</td><td>
<img src="http://farm5.static.flickr.com/4088/4952013489_58ecaef01c_t.jpg" width="100" height="79" alt="heraldSquare" />
</td></tr>

<tr><td>
Бродвея и Седьмой авеню - Таймс Сквер
</td><td>
<img src="http://farm5.static.flickr.com/4146/4952028317_7b962fc88c_t.jpg" width="100" height="75" alt="b1" />
</td></tr>

<tr><td>
Бродвея и Восьмой авеню - Коламбас Серкл (Columbus Circle)
</td><td>
<img src="http://farm5.static.flickr.com/4090/4952028405_0401e56841_t.jpg" width="100" height="75" alt="b2" />
</td></tr>

</table>
Кроме того, некоторые из улиц (Street), проходящих через те же самые перекрестки, в свою очередь приобрели звездный статус - кто не слышал, например, о Сорок второй улице, ныне респектабельной, а когда-то улицы Красных фонарей; или о Четырнадцатой, упомянутой выше.
Эти улицы образуют знакомую каждому нью-йоркцу последовательность
<br/>
<i><b>8, 14, 23, 28, 34, 42</b></i>. 
<br/>
Последовательность <s>чисел Фибоначчи по-манхэттенски</s> состоит из номеров улиц, на которых <a href="http://igornasa.livejournal.com/8772.html#SubwayMap" target="blank">любая линия метро обязательно имеет станцию</a>. 


<br/><br/>
<h3>Районы Манхэттена</h3>



Районов - около тридцати. Они сильно отличаются друг от друга размерами, архитектурой, обитателями и историей.
<br/>
Вот карта Манхэттена из путеводителя. На ней показаны основные районы со своими достопримечательностями:

<br/><br/>

<a href="http://www.flickr.com/photos/igornasa/4861925846/" title="ManhattanMap_North by igornasa, on Flickr"><img src="http://farm5.static.flickr.com/4073/4861925846_13ac64c051_b.jpg" width="567" height="1024" alt="ManhattanMap_North" /></a>

<a href="http://www.flickr.com/photos/igornasa/4861303065/" title="ManhattanMap_South by igornasa, on Flickr"><img src="http://farm5.static.flickr.com/4096/4861303065_745425b305_b.jpg" width="567" height="1024" alt="ManhattanMap_South" /></a>



<br/><br/>

Для быстрого восприятия эта карта слишком перегружена деталями, да и районы с их неправильными границами не складываются в простую схему. 
<br/><br/>
Более удобна упрощенная карта, в которой Манхэттен разделен на полосы, а в каждой полосе районы объединены в группы. В результате получается всего 16 "прямоугольников": 
<br/><br/>

<table class="my_table" width="567">
  <tr>
    <td>
<a href="http://www.flickr.com/photos/ivladimir/4933001690/" title="A_ManhattanDistricts by ivladimir, on Flickr"><img src="http://farm5.static.flickr.com/4101/4933001690_a29c4aee38_z.jpg" width="233" height="640" alt="A_ManhattanDistricts" /></a>
    </td>

    <td valign="top">
<b>





<i>Downtown:</i>
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
1 - Lower Manhattan
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
2 - Tribeca/SOHO
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
3 - China Town/ Little Italy
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
4 - Lower East Side
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
5 - Greenwich Village
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
6 - East Village
<br/>

<br/>

<i>Midtown</i>
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
7 - Chelsea/Garment District
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
8 - Gramercy/Murray Hill
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
9 - Midtown West
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
10 - Midtown East
<br/>
<br/>



<i>Uptown:</i>
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
11 - Upper West Side
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
12 - Upper East Side
<br/><br/>





<i>Upper Manhattan:</i>
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
13 - Morningside Heights
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
14 - Harlem
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
15 - Washington Heightes
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
16 - Fort George/Inwood
<br/><br/>

<i><b>Острова</b></i>
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
a - Liberty Island
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
b - Ellis Island
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
c - Governors Island
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;
d - Roosevelt Island
<br/>





</b>
    </td>
  </tr>
</table>

С этой картой мы и будем исследовать "Остров Сокровищ". 
 





<br/><br/>

<i><a href="http://igornasa.livejournal.com/10768.html">
Манхэттен. Переобитаемый остров</a></i>
<br/>

<i><a href="http://igornasa.livejournal.com/13709.html">
Манхэттен. История. Манна-хатта, Новый Амстердам, Нью-Йорк</a></i>


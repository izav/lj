﻿
Россетти, муж Элизабет Сиддал и идеолог братства прерафаэлитов, написал с нее множество картин, на нескольких она  изображена в образе Офелии, чью судьбу мистически повторила.
<br/><br/>








<i>&quot;Гамлет и Офелия&quot; Данте Габриэль Россетти 1858 год ( при жнизни Элизабет)</i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23451826840/in/dateposted-public/" title="Untitled"><img src="https://farm6.staticflickr.com/5822/23451826840_db11f1d8d3.jpg" width="350" alt="Untitled"></a>
<br/><br/><br/><br/>

<i>&quot;Первое сумашествие Офелии&quot; Данте Габриэль Россетти 1864 год (после смерти Элизабет)</i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23638984842/in/dateposted-public/" title="Untitled"><img src="https://farm6.staticflickr.com/5687/23638984842_e9b7dec89c.jpg" width="350" alt="Untitled"></a></div>
<br/><br/><br/><br/>

<i>&quot;Гамлет и Офелия&quot; Данте Габриэль Россетти 1858 год </i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23291364324/in/dateposted-public/" title="Untitled"><img src="https://farm6.staticflickr.com/5708/23291364324_f92d386209.jpg" width="400" alt="Untitled"></a>
<br/><br/>


Офелия теряет отца, у Лиззи рождается мертвый ребенoк.<br/>

Офелия от горя сходит с ума и поет странные песни. Лиззи качает пустую колыбель, призывая других не шуметь, чтобы не разбудить ребенка.<br/>

Офелия утонула, Лиззи умерла от предозировки.<br/>
Офелию хоронят <i>"так не по обряду"</i>, подозревая самоубийство. Про Лиззи ходят слухи, что она оставила мужу записку: <i>"Позаботься о моем брате".</i><br/>

Гамлет прыгнул в могилу Офелии и прямо на гробу несчастной девушки  устроил драку. Россетти,  всех шокировав, дал разрешение на вскрытие  могилы Лиззи.
<br/><br/>


<i>Литография к "Гамлету", Эжена Делакруа 1843 год</i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23837666771/in/dateposted-public/" title="Untitled"><img src="https://farm6.staticflickr.com/5804/23837666771_faca1a6ed6_z.jpg" width="400" alt="Untitled"></a>
<br/><br/><br/><br/><br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23920317755/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1585/23920317755_30e6356ce9.jpg" width="400" height="305" alt="Untitled"></a>

<br/><br/>

Bиня себя в смерти жены, Россетти  сделал романтический жест в духе прерафаэлитов -
поклялся навсегда оставить поэзию и похоронил Элизабет вместе с рукописями своих стихов. 
Спустя семь лет он сильно об этом пожалел.
Художник начал терять зрение и ему стало сложно писать картины. Тогда "по совету друзей" он решил вернуться к поэзии и извлечь из могилы Сиддал свои сочинения, чтобы опубликовать их с новыми стихами. Реальный Хитклифф не присутствовал при эксгумации, тайно ночью это сделали  доверенные люди. По легенде тело Лиззи оказалось нетленным, а роскошные рыжие волосы продолжали расти после смерти и заполнили собой весь гроб. 
<br/><br/>
Современные модные фотогрaфы до такого эффектного сюжета еще не добрались, но молодые художники уже пробуют свои силы в этом направлении - не для слабонервных <a href="https://farm1.staticflickr.com/714/23834600071_ffa26ca6c2_o.jpg">цифровая картина Ровины Кай</a>.
<br/><br/>
<i>Семейный склеп семьи Россетти на Хайгейтском кладбище<br/>
<a href="http://lizziesiddal.com/portal/photographs-of-elizabeth-siddals-grave/">Могила Элизабет Сиддал</a></i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23288858774/in/dateposted-public/" title="Untitled"><img src="https://farm6.staticflickr.com/5619/23288858774_e9a0f49fab.jpg" width="500" height="375" alt="Untitled"></a>
<br/><br/><br/>


Легенда о Лиззи-Рапунцель,  стала известна со слов <a href="https://en.wikipedia.org/wiki/Charles_Augustus_Howell">Чарльза Огастеса Хауэлла</a>, арт-агентa Россетти и секретаря Рёскина, <a href="http://lizziesiddal.com/portal/the-worst-man-in-london/">cамого ужасного человека Лондона</a>, чей образ стал прототипом злодея в рассказe Конан Дойля <a href="http://bukva.org.ua/donald-tomas-zabitie-dela-sherloka-holmsa.html?page=2">"Конец Чарльза Огастеса Милвертона"</a>. Это он уговорил Россетти на эксгумацию и был ее главным исполнителем.  ( Милвертон стал Магнуссеном в современном сериале "Шерлок" )<br/><br/>
Вскрытие могилы держалось в строжайшем секрете - родители  Элизабет так и умерли в неведении, но в ближний круг  Россетти шокирующию правда просочилась и несколько друзей  прекратили общение с художником.
<br/><br/>
<i> В могилy с Элизабет   позже были захоронены мать, сестра и брат  Россетти.</i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23808771092/in/dateposted-public/" title="Untitled"><img src="https://farm6.staticflickr.com/5755/23808771092_41e5a6a8e6.jpg" width="337" height="500" alt="Untitled"></a>
<br/><br/><br/><br/>
Есть версия, что  Брэм Стовер  использовал эту историю в "Дракуле". Писатель посвятил свой роман другу детства <a href="https://en.wikipedia.org/wiki/Hall_Caine">Холлу Кейну</a>, который был секретарем Россетти и после смерти патрона опубликовал  "Воспоминания  о Данте Габриэле Россетти", где  описал этот случай.

<br/><br/>

<i> Фильм Френсиса Капполы "Дракула" 1992 <br/>
Рыжеволосая Люси Вестерна превратится в  вампира и ее  могилa будет вскрыта.</i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23451732510/in/dateposted-public/" title="Untitled"><img src="https://farm1.staticflickr.com/645/23451732510_5233289d0e.jpg" width="500" height="388" alt="Untitled"></a>
<br/><br/><br/>




Трагическое и смешное всегда соседствуют рядом: в "Гамлете" были подвыпившие могильщики, философствующие за рытьем места последнего упокоения Офелии, а в истории с Элизабет Сиддал - черви, которые, к огромному разочарованию Россетти, съели его самую удачную поэму.  
<br/><br/>

Автор переписал yцелевшие произведения, а оригиналы  уничтожил.  Cпасенные сочинения были  изданы и даже при такой ауре имели некоторый успех. Любвeобильный художник каждой новой музе посвящaл что-нибудь стихотворное и в итоге оставил солидное литературное наследие. 

<br/><br/>

<i>Одна из дважды уцелевших страниц.</i>
<br/><br/>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23451870280/in/dateposted-public/" title="Untitled"><img src="https://farm1.staticflickr.com/594/23451870280_44610398db.jpg" width="300" alt="Untitled"></a>
<br/><br/><br/>
Россетти до конца дней винил себя, что лишил Элизабет счастья при жизни и покоя после смерти, и находил oблегчение в алкоголе и наркотиках.
Ему повсюду мерещилось присутствие   жены, а <a href="http://www.liveinternet.ru/users/vl866911/post293070435/">ночные крики домашнего енота</a> он принимал за  стоны ее призрака.  Понимая, что умирает,  велел похоронить себя не в семейном склепе семьи Россетти, где покоилась Сиддaл, а подальше от Хайгейта, внушавшего ему панический ужас.
<br/><br/><br/>
Слава   Данте Габриэля Россетти как поэта пришлась на эпоху символизма. По значимoсти его сонеты из сборника "Дом жизни",  куда вошли стихи извлеченные из могилы жены, критики сравнивали  с сонетами  Уильямa Шекспирa.
<br/><br/>

<i>"The House of Life"  Dante Gabriel Rossetti, <a href="http://www.elstonpress.com/ElstonHouseOfLife.html">издание 1901 года</a></i><br/><br/>






<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23747695235/in/dateposted-public/" title="Untitled"><img src="https://farm1.staticflickr.com/614/23747695235_9b2354c451.jpg" width="500" height="340" alt="Untitled"></a>
<br/><br/><br/><br/>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23665249791/in/dateposted-public/" title="Untitled"><img src="https://farm1.staticflickr.com/653/23665249791_b919567e0f.jpg" width="500" height="328" alt="Untitled"></a>
<br/><br/><br/><br/>




<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23451979420/in/dateposted-public/" title="Untitled"><img src="https://farm1.staticflickr.com/750/23451979420_d1bd66093d.jpg" width="500" height="332" alt="Untitled"></a>

<br/><br/><br/>
 
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/23119502364/in/dateposted-public/" title="Untitled"><img src="https://farm1.staticflickr.com/733/23119502364_dab2c3c939_o.jpg" width="436" height="550" alt="Untitled"></a>
<br/><br/><br/>










<i>
<a href="http://edu.znate.ru/docs/645/index-79858-1.html?page=2
">Литературные женские образы, как источники вдохновения для творчества прерафаэлитов.</a>
<br/>
<a href="http://hamletworks.net/YOUNG/index.html">Visual Representations of Hamlet, 1709-1900 </a>
<br/>

<a href="http://lizziesiddal.com">LizzieSiddal.com</a>
</i>
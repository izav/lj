﻿




<a target="_self" href="http://books.google.com/books?id=U14oAAAAYAAJ&amp;pg=PA319&amp;lpg=PA319&amp;dq=Tam+O%27Shanter+Eleazer+Hutchinson+Miller&amp;source=bl&amp;ots=tHwcJ9Msy7&amp;sig=lS0SAridiQXFZVFV06AAoZkNedY&amp;hl=en&amp;sa=X&amp;ei=ZvE7UbTPDoTy0wGku4D4Cw&amp;ved=0CDMQ6AEwAQ#v=onepage&amp;q=Tam%20O%27Shanter%20Eleazer%20Hutchinson%20Miller&amp;f=false" rel="nofollow"></a><h4><a target="_self" href="http://books.google.com/books?id=U14oAAAAYAAJ&amp;pg=PA319&amp;lpg=PA319&amp;dq=Tam+O%27Shanter+Eleazer+Hutchinson+Miller&amp;source=bl&amp;ots=tHwcJ9Msy7&amp;sig=lS0SAridiQXFZVFV06AAoZkNedY&amp;hl=en&amp;sa=X&amp;ei=ZvE7UbTPDoTy0wGku4D4Cw&amp;ved=0CDMQ6AEwAQ#v=onepage&amp;q=Tam%20O%27Shanter%20Eleazer%20Hutchinson%20Miller&amp;f=false" rel="nofollow"><b>4) Элизар  Хатчинсон Миллер</b></a></h4> 

<b>Иллюстрации,  1868 год</b><br><br>
<i>Иллюстрации были сфотографированы знаменитым <a target="_self" href="http://en.wikipedia.org/wiki/Alexander_Gardner_%28photographer%29" rel="nofollow">Александром Гарднером</a>  для  очень редкого издания "Тэма О'Шентера" 1868 года (осталось ~ 10 книг)</i>

<br>
<br><br>
<a href="http://farm9.staticflickr.com/8520/8541257055_d066cc1d97_b.jpg"><img src="http://farm9.staticflickr.com/8520/8541257055_d066cc1d97_c.jpg" alt="Untitled" height="800" width="575"></a>
<br><br><br><br><br>

<a href="http://farm9.staticflickr.com/8508/8541290063_94bdd8acb9_b.jpg"><img src="http://farm9.staticflickr.com/8508/8541290063_94bdd8acb9_z.jpg" alt="Untitled" height="464" width="640"></a>
<br><br><br><br><br>

<a href="http://farm9.staticflickr.com/8506/8542388382_f008dd4e19_b.jpg"><img src="http://farm9.staticflickr.com/8506/8542388382_f008dd4e19_z.jpg" alt="Untitled" height="461" width="640"></a>
<br><br><br><br><br>

<a href="http://farm9.staticflickr.com/8367/8541257997_b0aa4dcc6e_b.jpg"><img src="http://farm9.staticflickr.com/8367/8541257997_b0aa4dcc6e_z.jpg" alt="Untitled" height="463" width="640"></a>
<br><br><br><br>

<a href="http://farm9.staticflickr.com/8100/8542397442_da62bdc498_b.jpg"><img src="http://farm9.staticflickr.com/8100/8542397442_da62bdc498_z.jpg" alt="Untitled" height="464" width="640"></a>
<br><br><br><br><br>

<a href="http://farm9.staticflickr.com/8391/8541257545_cd80fcab4e_b.jpg"><img src="http://farm9.staticflickr.com/8391/8541257545_cd80fcab4e_z.jpg" alt="Untitled" height="457" width="640"></a>
<br><br><br><br><br><br>

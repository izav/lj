﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LJ.ViewModels.Blog
{
    public class EditPostViewModel
    {
        public  const int MaxShortDescription = 1000;
        public  const int MaxContent = 30000;
        public Post Post { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public IEnumerable<Group> FriendGroups { get; set; }

        [Display(Name = "Short Description counter")]
        [Range(1, MaxShortDescription, ErrorMessage = "html characters  must be in range [1 - 1,000]")]
        public int ShortDescriptionCounter { get; set; }

        [Display(Name = "Content counter")]
        [Range(1, MaxContent, ErrorMessage = "html characters  must be in range [1 - 30,000]")]
        public int ContentCounter { get; set; }

        [Display(Name = "Tags")]
        [Range(1, Int64.MaxValue, ErrorMessage = " count can't be zero")]
        public int PostTagsCount { get; set; }
    }
}
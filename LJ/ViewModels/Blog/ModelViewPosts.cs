﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels.User;
using LJ.ViewModels.Blog;

namespace LJ.ViewModels
{
    public class ModelViewPosts
    {
        public ModelViewPosts() 
        { 
        }

        public ModelViewPosts(IEnumerable<Post> posts)
        {
            Posts = posts;
        
        }
        public ModelViewSideBar ModelViewSideBar { get; set; }
        public UserPanelInfo UserPanelInfo { get; set; }
        public ApplicationUser User { get; set; }
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<ModelViewPostShort> PostsShort { get; set; }
        public int TotalPosts { get; set; }
        public int TotalComments { get; set; }
        public int TotalPages { get; set; }
        public int PageNumber { get; set; }
        public int PostsPerPage { get; set; }
        public bool IsNext { get; set; }
        public bool IsPrevious { get; set; }
        public Category Category { get; set; }
        public Tag Tag { get; set; }
        public Group Group { get; set; }
        public string Search { get; set; }

    }
}
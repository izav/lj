﻿using System.Web;
using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace LJ.Filters
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {           
            return  HttpContext.Current.User.IsInRole("Admin");
        }
    }
}